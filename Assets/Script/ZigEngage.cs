﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZigEngage : MonoBehaviour {

    //public GameObject InstantiatePerUser;
    //Dictionary<int, GameObject> objects = new Dictionary<int, GameObject>();

    public GameObject perspectiveCamera;

    void Start()
    {
        //perspectiveCamera = GameObject.FindGameObjectWithTag("Player");
    }

    void Zig_UserFound(ZigTrackedUser user)
    {
        //GameObject o = Instantiate(perspectiveCamera) as GameObject;
        //objects[user.Id] = o;
        user.AddListener(perspectiveCamera);
    }

    void Zig_UserLost(ZigTrackedUser user)
    {
        //Destroy(objects[user.Id]);
        //objects.Remove(user.Id);
    }
}
