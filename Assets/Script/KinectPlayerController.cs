using UnityEngine;
using System;
using System.Collections;

public class KinectPlayerController : MonoBehaviour {
	
	//Assignments for a bitmask to control which bones to look at and which to ignore
	public enum BoneMask
	{
		None = 0x0,
		Hip_Center = 0x1,
		Spine = 0x2,
		Shoulder_Center = 0x4,
		Head = 0x8,
		Shoulder_Left = 0x10,
		Elbow_Left = 0x20,
		Wrist_Left = 0x40,
		Hand_Left = 0x80,
		Shoulder_Right = 0x100,
		Elbow_Right = 0x200,
		Wrist_Right = 0x400,
		Hand_Right = 0x800,
		Hip_Left = 0x1000,
		Knee_Left = 0x2000,
		Ankle_Left = 0x4000,
		Foot_Left = 0x8000,
		Hip_Right = 0x10000,
		Knee_Right = 0x20000,
		Ankle_Right = 0x40000,
		Foot_Right = 0x80000,
	}

    public Vector3 offset = Vector3.zero;
    public Vector3 scale = Vector3.one;
    public float nearZ = -5.0f;
    public float farZ = -20.0f;
	public SkeletonWrapper sw;
	
	public GameObject Hip_Center;
	public GameObject Spine;
	public GameObject Shoulder_Center;
	public GameObject Head;
	public GameObject Shoulder_Left;
	public GameObject Elbow_Left;
	public GameObject Wrist_Left;
	public GameObject Hand_Left;
	public GameObject Shoulder_Right;
	public GameObject Elbow_Right;
	public GameObject Wrist_Right;
	public GameObject Hand_Right;
	public GameObject Hip_Left;
	public GameObject Knee_Left;
	public GameObject Ankle_Left;
	public GameObject Foot_Left;
	public GameObject Hip_Right;
	public GameObject Knee_Right;
	public GameObject Ankle_Right;
	public GameObject Foot_Right;
	
	private GameObject[] _bones; //internal handle for the bones of the model
    private Calibrator calibrator;
	
	public int player;
	public BoneMask Mask = BoneMask.Hip_Center;
	
	// Use this for initialization
	void Start () {
		//store bones in a list for easier access
		_bones = new GameObject[(int)Kinect.NuiSkeletonPositionIndex.Count] {Hip_Center, Spine, Shoulder_Center, Head,
			Shoulder_Left, Elbow_Left, Wrist_Left, Hand_Left,
			Shoulder_Right, Elbow_Right, Wrist_Right, Hand_Right,
			Hip_Left, Knee_Left, Ankle_Left, Foot_Left,
			Hip_Right, Knee_Right, Ankle_Right, Foot_Right};

        calibrator = new Calibrator();
	}
	
	// Update is called once per frame
	void Update () {


        float speed = .1f;
        if (Input.GetKey(KeyCode.N))
        {
            nearZ += speed;
        }
        else if (Input.GetKey(KeyCode.M))
        {
            nearZ -= speed;
        }

        if (Input.GetKey(KeyCode.F))
        {
            farZ += speed;
        }
        else if (Input.GetKey(KeyCode.G))
        {
            farZ -= speed;
        }

        if (Input.GetMouseButton(1))
        {
            offset.z += speed;
        }
        else if (Input.GetMouseButton(0))
        {
            offset.z -= speed;
        }

		if(player == -1) return;

		//update all of the bones positions
		if (sw.pollSkeleton())
		{
			for( int ii = 0; ii < (int)Kinect.NuiSkeletonPositionIndex.Count; ii++) {
                if (((uint)Mask & (uint)(1 << ii)) > 0)
                {
                    Vector3 kp = new Vector3(
                        sw.bonePos[player, ii].x * scale.x,
                        sw.bonePos[player, ii].y * scale.y,
                        sw.bonePos[player, ii].z * scale.z);
                    Vector3 wp = calibrator.KToP(new Vector3(kp.x, kp.y, kp.z));
                    //wp += offset;

                    //Debug.Log("(" +kp.x + ", " + kp.y + ", " + kp.z + ") => (" + wp.x + ", " + wp.y + ", " + wp.z + ")");
                    //Debug.Log(kp + " => " + wp);

                    Vector3 kinectPosition = calibrator.KToP(Vector3.zero);
                   
                    wp.z = ((wp.z / kinectPosition.z) * (farZ - nearZ)) + nearZ;

                    if (wp.z >= -1)
                    {
                        Debug.Log(wp);
                    }

                    _bones[ii].transform.localPosition = wp + offset;
                    return;
                }
			}
		}
	}
}
