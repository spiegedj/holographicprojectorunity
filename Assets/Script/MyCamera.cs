﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MyCamera : MonoBehaviour {

    // Z coordinate of the screne to project onto
    public Vector4 s;
    // Projection matrix set by editor
    private Matrix4x4 P;
    // Anomorphic Projection Matrix
    private Matrix4x4 A;
    // Attached Camera
    private Camera cam;

    // Transform of the player
    private Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        cam = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        if (player.transform.localPosition.z < 0)
        {
            transform.localPosition = transform.forward * (player.transform.localPosition.z + .1f);
        }


        float ar = cam.aspect;
        P = Matrix4x4.Ortho(-cam.orthographicSize * ar, cam.orthographicSize * ar, -cam.orthographicSize, cam.orthographicSize, cam.nearClipPlane, cam.farClipPlane + 5000);

        //float d = sZ - transform.position.z;
        Vector4 v = player.position;
        v.w = 1.0f;

        A = Anamophosis(cam.worldToCameraMatrix, v, s, GetComponent<Camera>().farClipPlane, GetComponent<Camera>().nearClipPlane);
        GetComponent<Camera>().projectionMatrix = P * A;
    }


    /**
      v: Position of the player
      sZ: the z coordinate of the screen to project onto
      d: the z distance from the screen to the camera. Assumed positive
      
      Since at this point the vertex coordinates are in camera space we need the distance from the screen 
      to the camera to fake a temporary conversion back to world coordinates. This implies that the camera x and y 
      are both 0.

      I used the equations from Distortion.cs and converted it to matrix form. This is what I ended up with:

      x' = x (v.z - sZ) / (v.z - z) + z (-v.x) / (v.z - sZ) + (sZ * v.x) / (v.z - z)
      y' = y (v.z - sZ) / (v.z - z) + z (-v.y) / (v.z - sZ) + (sZ * v.y) / (v.z - z)
      z' = z (v.z - f -n) / (v.z - z) + (f * n) / (v.z - z)
     
      I would have prefered z' = z but you can't do this with a matrix because you end up with a squared term. This is the 
      same problem that occurs with projection matrices so I used a similar trick.


      Matrix:  
      
      [v.z - sZ, 0       , -v.x       , sZ * v.x]
      [0       , v.z - sZ, -v.y       , sZ * v.y]
      [0       , 0       , v.z - f - n, f * n   ]
      [0       , 0       , -1         , v.z     ]       

    **/

    static Matrix4x4 Anamophosis(Matrix4x4 worldToCameraMatrix, Vector4 v, Vector4 s, float f, float n)
    {
        // Transform entered values into camera space. It is assumed that the camera is just shifted -d units from the origin on the z axis
        v = worldToCameraMatrix * v;
        //Vector4 s = new Vector4(0, 0, sZ, 1);
        float sZ = (worldToCameraMatrix * s).z;

        //Debug.Log(sZ);

        f = -f;
        n = -n;

        //Debug.Log("SZ: " + sZ + " v: " + v + " f: " + f + " n: " + n);

        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = v.z - sZ;
        m[0, 1] = 0;
        m[0, 2] = -v.x;
        m[0, 3] = sZ * v.x;

        m[1, 0] = 0;
        m[1, 1] = v.z - sZ;
        m[1, 2] = -v.y;
        m[1, 3] = sZ * v.y;

        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = (v.z - f - n);
        m[2, 3] = f * n;

        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = -1;
        m[3, 3] = (v.z);

        //Vector4 test = m * new Vector4(0, 0, sZ, 1);
        //Vector4 test2 = m * new Vector4(0, 0, sZ + 1, 1);
        //Debug.Log("Far: " + test / test.w + " Near: " + test2 / test2.w);

        return m;
    }

    // This one uses a direction vector from the player to the verteces that is the 
    // same for all vertices which causes incorrect distortion when using multiple objects in the same scene.
    static Matrix4x4 DirectionalAnamoprhosis(Vector3 dir, float sZ, float d)
    {

        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = 1;
        m[0, 1] = 0;
        m[0, 2] = -(dir.x / dir.z);
        m[0, 3] = (sZ * dir.x + d * dir.x) / dir.z;
        m[1, 0] = 0;
        m[1, 1] = 1;
        m[1, 2] = -(dir.y / dir.z);
        m[1, 3] = (sZ * dir.y + d * dir.y) / dir.z;
        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = 1;
        m[2, 3] = 0;
        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = 0;
        m[3, 3] = 1;
        return m;
    }
}
