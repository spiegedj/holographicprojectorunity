﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PlayerCam : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(Vector3.zero);
	}
}
