﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TunnelController : MonoBehaviour {

    public bool scroll = false;
    public float scrollSpeed = 5.0f;
    public GameObject right;
    public GameObject left;
    public GameObject top;
    public GameObject bottom;
    public float depth = 1000.0f;
    private Camera cam;



	// Use this for initialization
	void Start () {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        float height = cam.orthographicSize * 2;
        float width = cam.aspect * height;

        left.transform.localScale = new Vector3(height, depth, 1);
        left.transform.position = new Vector3(-width / 2.0f, 0, depth / 2.0f);

        right.transform.localScale = new Vector3(height, depth, 1);
        right.transform.position = new Vector3(width / 2.0f, 0, depth / 2.0f);

        top.transform.localScale = new Vector3(width, depth, 1);
        top.transform.position = new Vector3(0, height / 2.0f, depth / 2.0f);

        bottom.transform.localScale = new Vector3(width, depth, 1);
        bottom.transform.position = new Vector3(0, -height / 2.0f, depth / 2.0f);

        /*if (scroll)
        {
            float offset = Time.time * scrollSpeed;
            left.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0, offset);
            right.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0, -offset);
            top.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0, offset);
            bottom.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0, -offset);
        }*/

	}
}
