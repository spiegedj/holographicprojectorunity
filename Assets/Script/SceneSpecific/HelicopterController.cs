﻿using UnityEngine;
using System.Collections;

public class HelicopterController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time, 5) / 5.0f - 3.0f, transform.position.z);
	}
}
