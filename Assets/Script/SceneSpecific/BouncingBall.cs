﻿using UnityEngine;
using System.Collections;

public class BouncingBall : MonoBehaviour {

    public float speed;
    public Vector3 direction;

	// Use this for initialization
	void Start () {
        //direction = Random.onUnitSphere;
        direction = Vector3.forward + Random.onUnitSphere;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += direction * speed;
	}

    void OnTriggerEnter(Collider other)
    {
        direction = Vector3.Reflect(direction, other.transform.forward);
    }
}
