﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {

    public GameObject bullet;

	// Use this for initialization
	void Start () {
        Invoke("Shoot", Random.value);
	}

    void Shoot()
    {
        GameObject obj = Instantiate(bullet) as GameObject;
        obj.transform.parent = this.transform;
        obj.transform.localPosition = Vector3.zero;
        Invoke("Shoot", Random.value);
    }
	
	// Update is called once per frame
	void Update () {
	
	}


}
