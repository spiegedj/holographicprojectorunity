﻿using UnityEngine;
using System.Collections;

public class Distortion : MonoBehaviour {

    private Mesh mesh;
    private float screenZ;
    public GameObject cam;

    private Plane groundPlane;
    private Vector3 prevViewerPosition;
    private Vector3[] originalVertices;
	private Vector3 originalPosition;


    void Start()
    {
        //screenZ = transform.position.z - 5.0f;
        screenZ = 0.0f;
        mesh = GetComponent<MeshFilter>().mesh;
        cam = GameObject.FindGameObjectWithTag("Player");
        groundPlane = new Plane(new Vector3(0, 0, -1), new Vector3(0, 1, 0));
        originalVertices = mesh.vertices.Clone() as Vector3[];

		originalPosition = transform.position;
    }

    void Update()
    {
        Vector3 viewerPosition = cam.transform.position;
        if (viewerPosition == prevViewerPosition) return;


        Vector3[] vertices = mesh.vertices;
        for (int i = 0; i < mesh.vertexCount; i++)
        {
            Vector3 vertex = originalVertices[i];
            Vector3 worldVertex = transform.TransformPoint(vertex);

            /*Vector3 dir = worldVertex - viewerPosition;
            float t = ((screenZ -worldVertex.z) / dir.z);
            float x = worldVertex.x + t * dir.x;
            float y = worldVertex.y + t * dir.y;
            float z = worldVertex.z;*/

            float t = (screenZ - viewerPosition.z) / (viewerPosition.z - worldVertex.z);
            float x = viewerPosition.x + t * (viewerPosition.x - worldVertex.x);
            float y = viewerPosition.y + t * (viewerPosition.y - worldVertex.y);
            float z = worldVertex.z;

            Vector3 worldNewVertex = new Vector3(x, y, z);
            Vector3 newVertex = transform.InverseTransformPoint(worldNewVertex);

            vertices[i] = newVertex;
            //Debug.Log(vertex + " => " + newVertex);

        }
        mesh.vertices = vertices;
        mesh.RecalculateNormals();

        prevViewerPosition = viewerPosition;
    }
}
