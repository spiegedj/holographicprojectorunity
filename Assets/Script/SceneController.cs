﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Application.LoadLevel((Application.loadedLevel + 1) % Application.levelCount);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Application.LoadLevel((Application.loadedLevel - 1) % Application.levelCount);
        }
	}
}
