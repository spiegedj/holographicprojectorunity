﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Debug : MonoBehaviour {

    private Text text;
    public CalibrateController calibrator;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        string debug = "";
        debug += "Player: " + calibrator.trackedObject.transform.position;
        text.text = debug;
	}
}
