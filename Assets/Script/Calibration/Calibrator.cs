﻿//http://blog.3dsense.org/programming/kinect-projector-calibration-human-mapping-2/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;
using System.Linq;


public class Calibrator
{
    private Matrix coordMatrix;
    private Matrix coordVector;
    private Plane screen;

    private float[] q;
    private Vector3 A;
    private Vector3 B;
    private Vector3 C;
    private int qLength = 6;


    private List<Vector3> kinectCoords;

    public Calibrator()
    {
        try
        {
            ReadDataFromFile();
        } catch (Exception e) {
            Debug.Log(e);
            q = new float[qLength];
        }
    }

    public void setQ(float[] q)
    {
        this.q = q;
    }

    public bool Init(List<Vector3> kinectCoords, List<Vector2> projectorCoords)
    {
        prepareMatrices3(kinectCoords, projectorCoords);
        try
        {
            //Debug.Log(coordMatrix);
            //Debug.Log(coordVector);
            //Debug.Log(coordMatrix.QRDecomposition.Solve(coordVector));

            Matrix result = coordMatrix.QRDecomposition.Solve(coordVector);
            Matrix error = coordVector - (coordMatrix * result);
            Debug.Log("ERROR: " + error.GetArray()[0].Sum());
            double[][] data = coordMatrix.QRDecomposition.Solve(coordVector).GetArray();
            

            q = new float[qLength];
            for (int i = 0; i < q.Length; i++)
            {
                q[i] = (float)data[i][0];
            }

            A = kinectCoords[0];
            B = kinectCoords[kinectCoords.Count - 1];
            C = kinectCoords[(int) ((kinectCoords.Count - 1) / 2.0f)];
            screen = new Plane(A, B, C);

            WriteDataToFile(kinectCoords, projectorCoords);
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
        return true;
    }

    public Vector3 KToP(Vector3 kp)
    {
       Vector3 pp = new Vector3();
        //kp = kp - (screen.normal * screen.GetDistanceToPoint(kp));

        //pp.x = (float) (q[0] * kp.x + q[1] * kp.y + q[2] * kp.z + q[3]) / (q[8] * kp.x + q[9] * kp.y + q[10] * kp.z + 1);
        //pp.y = (float) (q[4] * kp.x + q[5] * kp.y + q[6] * kp.z + q[7]) / (q[8] * kp.x + q[9] * kp.y + q[10] * kp.z + 1);

        //pp.x = (float)(q[0] * kp.x + q[1] * kp.y + q[2] * kp.z + q[3]);
        //pp.y = (float)(q[4] * kp.x + q[5] * kp.y + q[6] * kp.z + q[7]);

       pp.x = (float)(q[0] * kp.x + q[1] * kp.y + q[2]);
       pp.y = (float)(q[3] * kp.x + q[4] * kp.y + q[5]);
       pp.z = distanceFromScreen(kp);

        return pp;
    }

    public float distanceFromScreen(Vector3 kp)
    {
        return screen.GetDistanceToPoint(kp);
    }

    private void prepareMatrices3(List<Vector3> kinectCoords, List<Vector2> projectorCoords)
    {
        double[][] mData = Matrix.CreateMatrixData(projectorCoords.Count * 2, 6);
        double[][] vData = Matrix.CreateMatrixData(projectorCoords.Count * 2, 1);

        for (int i = 0; i < projectorCoords.Count * 2; i += 2)
        {
            Vector3 kc = kinectCoords[i / 2];
            Vector2 projC = projectorCoords[i / 2];
            mData[i][0] = kc.x;
            mData[i][1] = kc.y;
            mData[i][2] = 1;
            mData[i][3] = 0;
            mData[i][4] = 0;
            mData[i][5] = 0;
            vData[i][0] = projC.x;

            mData[i + 1][0] = 0;
            mData[i + 1][1] = 0;
            mData[i + 1][2] = 0;
            mData[i + 1][3] = kc.x;
            mData[i + 1][4] = kc.y;
            mData[i + 1][5] = 1;
            vData[i + 1][0] = projC.y;
        }

        coordMatrix = new Matrix(mData);
        coordVector = new Matrix(vData);
    }

    private void prepareMatrices(List<Vector3> kinectCoords, List<Vector2> projectorCoords)
    {
        double[][] mData = Matrix.CreateMatrixData(projectorCoords.Count * 2, 8);
        double[][] vData = Matrix.CreateMatrixData(projectorCoords.Count * 2, 1);

        for (int i = 0; i < projectorCoords.Count * 2; i += 2)
        {
            Vector3 kc = kinectCoords[i / 2];
            Vector2 projC = projectorCoords[i / 2];
            mData[i][0] = kc.x;
            mData[i][1] = kc.y;
            mData[i][2] = kc.z;
            mData[i][3] = 1;
            mData[i][4] = 0;
            mData[i][5] = 0;
            mData[i][6] = 0;
            mData[i][7] = 0;
            vData[i][0] = projC.x;

            mData[i + 1][0] = 0;
            mData[i + 1][1] = 0;
            mData[i + 1][2] = 0;
            mData[i + 1][3] = 0;
            mData[i + 1][4] = kc.x;
            mData[i + 1][5] = kc.y;
            mData[i + 1][6] = kc.z;
            mData[i + 1][7] = 1;
            vData[i + 1][0] = projC.y;
        }

        coordMatrix = new Matrix(mData);
        coordVector = new Matrix(vData);
    }

    private void prepareMatrices2(List<Vector3> kinectCoords, List<Vector2> projectorCoords)
    {
        double[][] mData = Matrix.CreateMatrixData(projectorCoords.Count * 2, 11);
        double[][] vData = Matrix.CreateMatrixData(projectorCoords.Count * 2, 1);

        for (int i = 0; i < projectorCoords.Count * 2; i += 2)
        {
            Vector3 kc = kinectCoords[i / 2];
            Vector2 projC = projectorCoords[i / 2];
            mData[i][0] = kc.x;
            mData[i][1] = kc.y;
            mData[i][2] = kc.z;
            mData[i][3] = 1;
            mData[i][4] = 0;
            mData[i][5] = 0;
            mData[i][6] = 0;
            mData[i][7] = 0;
            mData[i][8] = -projC.x * kc.x;
            mData[i][9] = -projC.x * kc.y;
            mData[i][10] = -projC.x * kc.z;
            vData[i][0] = projC.x;

            mData[i + 1][0] = 0;
            mData[i + 1][1] = 0;
            mData[i + 1][2] = 0;
            mData[i + 1][3] = 0;
            mData[i + 1][4] = kc.x;
            mData[i + 1][5] = kc.y;
            mData[i + 1][6] = kc.z;
            mData[i + 1][7] = 1;
            mData[i + 1][8] = -projC.y * kc.x;
            mData[i + 1][9] = -projC.y * kc.y;
            mData[i + 1][10] = -projC.y * kc.z;
            vData[i + 1][0] = projC.y;
        }

        coordMatrix = new Matrix(mData);
        coordVector = new Matrix(vData);
    }

    void WriteDataToFile(List<Vector3> kinectCoords, List<Vector2> projectorCoords)
    {
        List<string> lines = new List<string>();
        for (int i = 0; i < q.Length; i++)
        {
            lines.Add(q[i] + "");
        }
        lines.Add(A.x + "");
        lines.Add(A.y + "");
        lines.Add(A.z + "");
        lines.Add(B.x + "");
        lines.Add(B.y + "");
        lines.Add(B.z + "");
        lines.Add(C.x + "");
        lines.Add(C.y + "");
        lines.Add(C.z + "");
        
        lines.Add("/////////////////////////////////////////");
        lines.Add("Kinect Coords    Projector Coords");
        for (int i = 0; i < kinectCoords.Count; i++)
        {
            lines.Add(kinectCoords[i].ToString() + " => " + projectorCoords[i].ToString());
        }
        System.IO.File.WriteAllLines("calibrationdata.txt", lines.ToArray());
    }

    void ReadDataFromFile()
    {
        string[] lines = System.IO.File.ReadAllLines("calibrationdata.txt");
        q = new float[qLength];
        for (int i = 0; i < q.Length; i++)
        {
            q[i] = float.Parse(lines[i]);
        }
        int l = q.Length;
        Vector3 A = new Vector3(float.Parse(lines[l]), float.Parse(lines[l + 1]), float.Parse(lines[l + 2]));
        Vector3 B = new Vector3(float.Parse(lines[l + 3]), float.Parse(lines[l + 4]), float.Parse(lines[l + 5]));
        Vector3 C = new Vector3(float.Parse(lines[l + 6]), float.Parse(lines[l + 7]), float.Parse(lines[l + 8]));

        screen = new Plane(A, B, C);
    }
}
