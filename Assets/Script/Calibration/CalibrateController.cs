﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CalibrateController : MonoBehaviour {
    private static CalibrateController instance;

    public GameObject trackedObject;
    public GameObject calibrationPointPrefab;
    public int width = 3;
    public int height = 2;

    private Calibrator calibrator;

    public List<Vector3> kinectCoords;
    public List<Vector2> projectorCoords;

    public GameObject[] screenPoints;
    private int index = 0;
    private bool isCalibrated = false;

    public bool Calibrating;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () {
        trackedObject.transform.localScale = trackedObject.transform.localScale * 1.0f;
        trackedObject.GetComponent<MeshRenderer>().material.color = Color.blue;

        kinectCoords = new List<Vector3>();
        projectorCoords = new List<Vector2>();
        screenPoints = new GameObject[width * height];

        float screenWidth = Camera.main.pixelWidth;
        float screenHeight = Camera.main.pixelHeight;

        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                Vector3 sp = new Vector3(i * (screenWidth / (width - 1)), j * (screenHeight / (height - 1)), 0.0f);
                Vector3 wp = Camera.main.ScreenToWorldPoint(sp);
                wp.z = 0.0f;

                GameObject calibrationPoint = Instantiate(calibrationPointPrefab);
                calibrationPoint.transform.parent = this.transform;
                calibrationPoint.transform.position = wp;
                screenPoints[(j * width) + i] = calibrationPoint;
            }
        }

        /*for (int i = 0; i < width * height; i++)
        {
            GameObject calibrationPoint = Instantiate(calibrationPointPrefab);
            calibrationPoint.transform.parent = this.transform;

            Vector3 pos = screenPoints[i].transform.position;
            pos.z = -5.0f;
            calibrationPoint.transform.position = pos;
            screenPoints[width * height + i] = calibrationPoint; 
        }*/

        foreach (GameObject screenPoint in screenPoints)
        {
            screenPoint.SetActive(false);
        }
        screenPoints[0].SetActive(true);

        /*kinectCoords.Add(new Vector3(0.0f, 0.5f, 1.4f));
        kinectCoords.Add(new Vector3(-0.2f, 0.5f, 1.4f));
        kinectCoords.Add(new Vector3(-0.4f, 0.5f, 1.4f));
        kinectCoords.Add(new Vector3(-0.6f, 0.5f, 1.4f));

        kinectCoords.Add(new Vector3(0.0f, 0.7f, 1.4f));
        kinectCoords.Add(new Vector3(-0.2f, 0.7f, 1.4f));
        kinectCoords.Add(new Vector3(-0.4f, 0.7f, 1.4f));
        kinectCoords.Add(new Vector3(-0.6f, 0.7f, 1.4f));

        kinectCoords.Add(new Vector3(0.0f, 0.9f, 1.4f));
        kinectCoords.Add(new Vector3(-0.2f, 0.9f, 1.41f));
        kinectCoords.Add(new Vector3(-0.4f, 0.9f, 1.41f));
        kinectCoords.Add(new Vector3(-0.6f, 0.9f, 1.41f));*/
	}
	
	// Update is called once per frame
	void Update () {
        if (!isCalibrated)
        {
            if (Input.GetMouseButtonDown(0))
            {
                kinectCoords.Add(trackedObject.transform.position);
                Vector3 sp = screenPoints[index].transform.position;
                //kinectCoords.Add(sp);
                projectorCoords.Add(sp);

                screenPoints[index].SetActive(false);
                index++;
                if (index >= screenPoints.Length)
                {
                    isCalibrated = true;
                    calibrator = new Calibrator();
                    isCalibrated = calibrator.Init(kinectCoords, projectorCoords);

                    if (isCalibrated)
                    {
                        for (int i = 0; i < screenPoints.Length; i++)
                        {
                            screenPoints[i].transform.position = calibrator.KToP(kinectCoords[i]);
                            screenPoints[i].transform.position = new Vector3(screenPoints[i].transform.position.x, screenPoints[i].transform.position.y, 0);
                            screenPoints[i].SetActive(true);
                            screenPoints[i].GetComponent<MeshRenderer>().material.color = Color.green;
                        }
                    }
                }
                else
                {
                    screenPoints[index].SetActive(true);
                }
            }
        }
	}
}
